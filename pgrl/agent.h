#pragma once
#include "network.h"
#include <vector>

struct State
{
	Matrix output;
	double reward;
};


class Agent :
	public Network
{
public:
	Agent(unsigned nLayers, unsigned* layerSizes);
	~Agent();

	unsigned stepLearn();
	void reward(double value);

	void learn();

private:

	unsigned pickAction();

private:

	unsigned pathNumber_;
	unsigned pathBatchSize_;

	double learningRate_;
	double discountFactor_;

	Matrix* deltaWeights_;
	Matrix* deltaBiases_;

	Matrix* trajectoryDeltaWeights_;
	Matrix* trajectoryDeltaBiases_;



	std::vector<State> states_;
};

