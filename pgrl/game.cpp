#include "game.h"


//-------------------------------------------------------------
Game::Game() :
	screenWidth_(200),
	screenHeight_(int((9.0/7.0) * float(screenWidth_))),


	movementForce_(float(screenWidth_)/ 200.0),

	ballRadius_(float(screenWidth_) / 35.0),
	ballPos_(screenWidth_ / 2),

	obstacleWidth_(float(screenWidth_) / 3.5),
	obstacleSpeed_(float(screenWidth_) / 1400.0),

	window_(nullptr),

	obstacle_(sf::LinesStrip, 2),
	ball_(ballRadius_)
{
	window_ = new sf::RenderWindow(sf::VideoMode(screenWidth_, screenHeight_), "Human game");
}


//-------------------------------------------------------------
Game::~Game()
{
	delete window_;
	window_ = nullptr;
}



//-------------------------------------------------------------
unsigned Game::playHuman()
{
	reset();

	unsigned counter = 0;
	while (!hit())
	{
		if (counter % 10 == 0)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{
				window_->close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			{
				ballPos_ -= movementForce_;
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			{
				ballPos_ += movementForce_;
			}
		}
		

		move();
		draw();

		counter++;
	}

	
	return counter/ 10;
}


//-------------------------------------------------------------
unsigned Game::playLearn(Agent& agent, bool display)
{
	reset();


	double lastHole = 0;
	double perfectDelta = obstaclePos_[0].x - lastHole;


	double* inputData = agent.getInputPtr();

	unsigned counter = 0;
	while (!hit())
	{
		if (counter % 10 == 0)
		{
			

			unsigned closest = 0;
			if (obstaclePos_[1].y < obstaclePos_[0].y)
			{
				closest = 1;
			}

			inputData[0] = (double)(ballPos_ - obstaclePos_[closest].x);
			inputData[1] = (double)obstaclePos_[closest].y;

			unsigned action = agent.stepLearn();

			if (action == 0)
			{
				ballPos_ -= movementForce_;

			}
			if (action == 1)
			{
				ballPos_ += movementForce_;

			}
		}

		if (move())
		{
			lastHole = ballPos_;

			unsigned closest = 0;
			if (obstaclePos_[1].y < obstaclePos_[0].y)
			{
				closest = 1;
			}

			perfectDelta = obstaclePos_[closest].x - ballPos_;


			agent.reward(1.0);
		}


		if (display)
		{
			draw();
		}
		
		counter++;
	}
	
	counter /= 10;

	
	double finalReward = 0;

	double delta = ballPos_ - lastHole;
	if (delta * perfectDelta < 0)
	{
		finalReward = -1.0 / abs(delta / double(screenWidth_));
	}
	else
	{
		delta = abs(delta);
		perfectDelta = abs(perfectDelta);

		finalReward = (1.0 / (abs(perfectDelta - delta) / perfectDelta)) - 1.0;

		if (delta > perfectDelta)
		{
			finalReward *= -1;
		}

	}
	agent.reward(finalReward);

	//std::cout << "\t\t" << finalReward << std::endl;

	agent.learn();

	return counter;
}



//-------------------------------------------------------------
bool Game::hit()
{

	for (unsigned i = 0; i < 2; i++)
	{
		if (abs(obstaclePos_[i].y) < 2 * ballRadius_)
		{
			if (abs(obstaclePos_[i].x - ballPos_) > (obstacleWidth_ / 2) - ballRadius_)
			{
				return true;
			}
		}
	}

	return false;
}



//-------------------------------------------------------------
bool Game::move()
{
	bool passed = false;
 	for (unsigned i = 0; i < 2; i++)
	{
		obstaclePos_[i].y -= obstacleSpeed_;

		if (obstaclePos_[i].y <= -ballRadius_)
		{
			passed = true;

			int randomModulo = screenWidth_ - 2 * obstacleWidth_;

			obstaclePos_[i].x = float(rand() % (randomModulo + 1)) - float(randomModulo / 2);
			obstaclePos_[i].y = 2 * (screenHeight_ - ballRadius_);
		}
	}

	if (ballPos_ < -(screenWidth_ / 2 - ballRadius_))
	{
		ballPos_ = -(screenWidth_ / 2 - ballRadius_);
	}
	if (ballPos_ > (screenWidth_ / 2 - ballRadius_))
	{
		ballPos_ = (screenWidth_ / 2 - ballRadius_);
	}

	return passed;
}



//-------------------------------------------------------------
void Game::draw()
{
	window_->clear();

	ball_.setPosition(sf::Vector2f(screenWidth_ / 2 + ballPos_ - ballRadius_, screenHeight_ - 2 * ballRadius_));
	window_->draw(ball_);

	for (unsigned i = 0; i < 2; i++)
	{
		obstacle_[0].position = sf::Vector2f(0, (screenHeight_ - ballRadius_) - obstaclePos_[i].y);
		obstacle_[1].position = sf::Vector2f(screenWidth_ / 2 + obstaclePos_[i].x - obstacleWidth_ / 2, (screenHeight_ - ballRadius_) - obstaclePos_[i].y);
		window_->draw(obstacle_);

		obstacle_[0].position = sf::Vector2f(screenWidth_, (screenHeight_ - ballRadius_) - obstaclePos_[i].y);
		obstacle_[1].position = sf::Vector2f(screenWidth_ / 2 + obstaclePos_[i].x + obstacleWidth_ / 2, (screenHeight_ - ballRadius_) - obstaclePos_[i].y);
		window_->draw(obstacle_);
	}
	
	

	window_->display();
}



//-------------------------------------------------------------
void Game::reset()
{
	ballPos_ = 0;
	
	obstaclePos_[0].x = -screenWidth_ / 4;
	obstaclePos_[0].y = screenHeight_ - ballRadius_;

	obstaclePos_[1].x = screenWidth_ / 4;
	obstaclePos_[1].y = 2 * (screenHeight_ - ballRadius_);

}



//-------------------------------------------------------------
float len(const sf::Vector2f& vector)
{
	return sqrtf(vector.x * vector.x + vector.y * vector.y);
}
