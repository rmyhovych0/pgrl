#pragma once

#include "matrix.h"



class Network
{
public:

	Network(unsigned nLayers, unsigned* layerSizes);
	~Network();

	double* getInputPtr();
	double* getOutputPtr();

	void feedforward();


protected:

	void backpropagate(Matrix* deltaWeights, Matrix* deltaBiases, 
		Matrix& lastLayerDelta);

protected:

	unsigned nLayers_;

	Matrix inLayer_;
	Matrix outLayer_;

	Matrix* weights_;
	Matrix* biases_;

};

