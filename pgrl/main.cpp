#include <iostream>
#include <fstream>
#include <string>

#include "agent.h"
#include "game.h"

#define PI 3.14159265359

using namespace std;


int main()
{

	Game game;
	unsigned layers[] = { 2, 30, 2 };

	Agent agent(3, layers);
	
	unsigned fitness = 0;

	bool visible = false;

	for (unsigned i = 0; i < 300000; i++)
	{
		
		if (i >= 80)
		{
			visible = true;
		}

		fitness += game.playLearn(agent, visible);
		//std::cout << "\n\n";
		//game.playHuman();

		
		if (i % 10 == 0 && i != 0)
		{
			cout << float(fitness) / 10.0 << endl;
			fitness = 0;
		}
		
	}

	fitness = 0;
		
}