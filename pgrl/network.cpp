
#include "network.h"


Network::Network(unsigned nLayers, unsigned* layerSizes) :
	nLayers_(nLayers),

	inLayer_(Matrix(1, layerSizes[0], 0)),
	outLayer_(Matrix(1, layerSizes[nLayers - 1], 0)),

	weights_(nullptr),
	biases_(nullptr)
{
	weights_ = new Matrix[nLayers_ - 1];
	biases_ = new Matrix[nLayers_ - 1];

	for (unsigned i = 0; i < nLayers_ - 1; i++)
	{
		weights_[i].setSize(layerSizes[i], layerSizes[i + 1]);
		biases_[i].setSize(1, layerSizes[i + 1]);
	}
}


Network::~Network()
{
	delete[] weights_;
	delete[] biases_;
}



double* Network::getInputPtr()
{
	return inLayer_.values_[0];
}



double* Network::getOutputPtr()
{
	return outLayer_.values_[0];
}



void Network::feedforward()
{
	Matrix ffMatrix = inLayer_;

	for (unsigned i = 0; i < nLayers_ - 1; i++)
	{
		ffMatrix = (ffMatrix * weights_[i]);
		ffMatrix += biases_[i];

		if (i == nLayers_ - 2)
		{
			ffMatrix.softmax();
		}
		else
		{
			ffMatrix.sigmoid();
		}
	}

	for (unsigned i = 0; i < ffMatrix.xSize_; i++)
	{
		outLayer_.values_[0][i] = ffMatrix.values_[0][i];
	}

}

void Network::backpropagate(Matrix* deltaWeights, Matrix* deltaBiases, Matrix& lastLayerDelta)
{
	//	creating activations and the sigmoid primed "z"s for every layer for the gradient calculations
	Matrix* sigmoidPrimedZs = new Matrix[nLayers_];
	Matrix* activations = new Matrix[nLayers_];


	//	filling up the activations and the sigmoid primed "z"s
	activations[0] = inLayer_;

	for (unsigned i = 1; i < nLayers_; i++)
	{
		activations[i] = (activations[i - 1] * weights_[i - 1]);
		activations[i] += biases_[i - 1];

		sigmoidPrimedZs[i] = activations[i];

		if (i == nLayers_ - 1)
		{
			activations[i].softmax();
		}
		else
		{
			sigmoidPrimedZs[i].sigmoidPrime();
			activations[i].sigmoid();
		}
	}


	//	calculating the last layer's gradients
	int deltaLayer = nLayers_ - 2;

	Matrix delta = lastLayerDelta;
	
	deltaWeights[deltaLayer] = activations[deltaLayer].transpose() * delta;
	deltaBiases[deltaLayer] = delta;

	deltaLayer--;


	//	calculating all the other layers' gradients
	for (; deltaLayer >= 0; deltaLayer--)
	{
		delta = (delta * weights_[deltaLayer + 1].transpose()).times(sigmoidPrimedZs[deltaLayer + 1]);

		deltaWeights[deltaLayer] = activations[deltaLayer].transpose() * delta;
		deltaBiases[deltaLayer] = delta;
	}


	//	clearing the memory
	delete[] activations;
	delete[] sigmoidPrimedZs;
}
