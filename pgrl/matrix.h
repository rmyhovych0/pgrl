#pragma once

#include <iostream>

class Matrix
{
public:
	Matrix();
	Matrix(unsigned ySize, unsigned xSize);
	Matrix(unsigned ySize, unsigned xSize, double value);
	Matrix(const Matrix& matrix);

	~Matrix();


	void setSize(unsigned ySize, unsigned xSize);
	void setSize(unsigned ySize, unsigned xSize, double value);


	Matrix& operator=(const Matrix& matrix);


	Matrix transpose();

	Matrix operator*(const Matrix& matrix) const;
	Matrix operator*(double value);

	Matrix& operator*=(double value);


	Matrix& operator+=(const Matrix& matrix);
	Matrix operator-(const Matrix& matrix) const;
	Matrix& operator-=(double value);

	Matrix& times(const Matrix& matrix);

	Matrix& logarithm();
	Matrix& invert();

	void softmax();
	void softmaxPrime();

	void sigmoid();
	void sigmoidPrime();

	friend Matrix operator-(double value, Matrix& matrix);
	
	friend class Network;
	friend class Agent;

private:

	double sigmoidFn(double value);

private:

	unsigned ySize_;
	unsigned xSize_;

	double** values_;

};

