#include "matrix.h"

#include <random>
#include <exception>

std::default_random_engine generator;
std::normal_distribution<double> distribution(0.0, 1.0);


Matrix::Matrix() :
	ySize_(0),
	xSize_(0),

	values_(nullptr)
{
}

Matrix::Matrix(unsigned ySize, unsigned xSize) :
	ySize_(ySize),
	xSize_(xSize),

	values_(nullptr)
{
	values_ = new double*[ySize_];

	for (unsigned y = 0; y < ySize_; y++)
	{
		values_[y] = new double[xSize_];

		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = distribution(generator) / sqrt(ySize_);
		}
	}
}

Matrix::Matrix(unsigned ySize, unsigned xSize, double value) :
	ySize_(ySize),
	xSize_(xSize),

	values_(nullptr)
{
	values_ = new double*[ySize_];

	for (unsigned y = 0; y < ySize_; y++)
	{
		values_[y] = new double[xSize_];

		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = value;
		}
	}
}

Matrix::Matrix(const Matrix& matrix) :
	xSize_(matrix.xSize_),
	ySize_(matrix.ySize_),

	values_(nullptr)
{
	values_ = new double*[ySize_];

	for (unsigned y = 0; y < ySize_; y++)
	{
		values_[y] = new double[xSize_];

		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = matrix.values_[y][x];
		}
	}
}



Matrix::~Matrix()
{
	for (unsigned y = 0; y < ySize_; y++)
	{
		delete[] values_[y];
		values_[y] = nullptr;
	}

	delete[] values_;
	values_ = nullptr;
}



void Matrix::setSize(unsigned ySize, unsigned xSize)
{
	ySize_ = ySize;
	xSize_ = xSize;


	values_ = new double*[ySize_];

	for (unsigned y = 0; y < ySize_; y++)
	{
		values_[y] = new double[xSize_];
	
		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = distribution(generator) / sqrt(ySize_);
		}
	}
}

void Matrix::setSize(unsigned ySize, unsigned xSize, double value)
{
	ySize_ = ySize;
	xSize_ = xSize;


	values_ = new double*[ySize_];

	for (unsigned y = 0; y < ySize_; y++)
	{
		values_[y] = new double[xSize_];

		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = value;
		}
	}
}






Matrix& Matrix::operator=(const Matrix& matrix)
{
	if (this != &matrix)
	{
		for (unsigned y = 0; y < ySize_; y++)
		{
			delete[] values_[y];
			values_[y] = nullptr;
		}

		delete[] values_;
		values_ = nullptr;



		xSize_ = matrix.xSize_;
		ySize_ = matrix.ySize_;



		values_ = new double*[ySize_];

		for (unsigned y = 0; y < ySize_; y++)
		{
			values_[y] = new double[xSize_];

			for (unsigned x = 0; x < xSize_; x++)
			{
				values_[y][x] = matrix.values_[y][x];
			}
		}

	}
	
	return *this;
}



Matrix Matrix::transpose()
{
	Matrix newMatrix = Matrix(xSize_, ySize_, 0);

	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			newMatrix.values_[x][y] = values_[y][x];
		}
	}

	return newMatrix;
}





Matrix Matrix::operator*(const Matrix& matrix) const
{
	if (this->xSize_ != matrix.ySize_)
	{
		throw std::logic_error("Can not cross product matrices\n");
	}


	Matrix newMatrix(this->ySize_, matrix.xSize_, 0);

	for (unsigned y = 0; y < newMatrix.ySize_; y++)
	{
		for (unsigned x = 0; x < newMatrix.xSize_; x++)
		{
			double value = 0;

			for (unsigned i = 0; i < this->xSize_; i++)
			{
				value += this->values_[y][i] * matrix.values_[i][x];
			}

			newMatrix.values_[y][x] = value;
		}
	}

	return newMatrix;
}

Matrix Matrix::operator*(double value)
{
	Matrix newMatrix = *this;

	for (unsigned y = 0; y < newMatrix.ySize_; y++)
	{
		for (unsigned x = 0; x < newMatrix.xSize_; x++)
		{
			newMatrix.values_[y][x] *= value;
		}
	}

	return newMatrix;
}

Matrix& Matrix::operator*=(double value)
{
	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] *= value;
		}
	}

	return *this;
}



Matrix& Matrix::operator+=(const Matrix& matrix)
{
	if (this->xSize_ != matrix.xSize_ &&
		this->ySize_ != matrix.ySize_)
	{
		throw std::logic_error("Can not add matrices\n");
	}


	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] += matrix.values_[y][x];
		}
	}

	return *this;
}

Matrix Matrix::operator-(const Matrix& matrix) const
{
	if (this->xSize_ != matrix.ySize_ &&
		this->ySize_ != matrix.ySize_)
	{
		throw std::logic_error("Can not subtract matrices\n");
	}

	Matrix newMatrix(ySize_, xSize_, 0);

	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			newMatrix.values_[y][x] = this->values_[y][x] - matrix.values_[y][x];
		}
	}

	return newMatrix;
}

Matrix& Matrix::operator-=(double value)
{
	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] -= value;
		}
	}

	return *this;
}

Matrix& Matrix::times(const Matrix& matrix)
{
	if (this->xSize_ != matrix.ySize_ &&
		this->ySize_ != matrix.ySize_)
	{
		throw std::logic_error("Can not dot matrices\n");
	}

	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] *= matrix.values_[y][x];
		}
	}

	return *this;
}

Matrix& Matrix::logarithm()
{
	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = log(values_[y][x]);
		}
	}

	return *this;
}

Matrix& Matrix::invert()
{
	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			if (values_[y][x] == 0)
			{
				values_[y][x] = 0.0001;
			}
			values_[y][x] = 1.0 / (values_[y][x]);
		}
	}

	return *this;
}



void Matrix::softmax()
{
	for (unsigned y = 0; y < ySize_; y++)
	{
		double expSum = 0;

		for (unsigned x = 0; x < xSize_; x++)
		{
			expSum += exp(values_[y][x]);
		}

		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = exp(values_[y][x]) / expSum;
		}
	}
}

void Matrix::softmaxPrime()
{
	softmax();

	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = values_[y][x] * (1.0 - values_[y][x]);
		}
	}
}

void Matrix::sigmoid()
{
	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			values_[y][x] = sigmoidFn(values_[y][x]);
		}
	}
}

void Matrix::sigmoidPrime()
{
	for (unsigned y = 0; y < ySize_; y++)
	{
		for (unsigned x = 0; x < xSize_; x++)
		{
			double spValue = sigmoidFn(values_[y][x]);

			values_[y][x] = spValue * (1.0 - spValue);
		}
	}
}

double Matrix::sigmoidFn(double value)
{

	return 1.0 / (1.0 + exp(-value));
}

Matrix operator-(double value, Matrix& matrix)
{
	Matrix newMatrix = matrix;

	for (unsigned y = 0; y < matrix.ySize_; y++)
	{
		for (unsigned x = 0; x < matrix.xSize_; x++)
		{
			matrix.values_[y][x] = value - matrix.values_[y][x];
		}
	}

	return newMatrix;
}
