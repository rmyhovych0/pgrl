#include <random>

#include "agent.h"



Agent::Agent(unsigned nLayers, unsigned* layerSizes) :
	Network(nLayers, layerSizes),

	pathNumber_(0),
	pathBatchSize_(1),

	learningRate_(0.05),
	discountFactor_(0.95)

{
	deltaWeights_ = new Matrix[nLayers_ - 1];
	deltaBiases_ = new Matrix[nLayers_ - 1];

	trajectoryDeltaWeights_ = new Matrix[nLayers_ - 1];
	trajectoryDeltaBiases_ = new Matrix[nLayers_ - 1];


	for (unsigned i = 0; i < nLayers_ - 1; i++)
	{
		deltaWeights_[i].setSize(weights_[i].ySize_, weights_[i].xSize_, 0.0);
		deltaBiases_[i].setSize(biases_[i].ySize_, biases_[i].xSize_, 0.0);

		trajectoryDeltaWeights_[i] = deltaWeights_[i];
		trajectoryDeltaBiases_[i] = deltaBiases_[i];
	}
}





Agent::~Agent()
{
	delete[] deltaWeights_;
	delete[] deltaBiases_;

	delete[] trajectoryDeltaWeights_;
	delete[] trajectoryDeltaBiases_;
}






unsigned Agent::stepLearn()
{
	feedforward();
	unsigned action = pickAction();

	Matrix stateAction = outLayer_;
	
	for (unsigned iAction = 0; iAction < stateAction.xSize_; iAction++)
	{
		if (iAction != action)
		{
			stateAction.values_[0][iAction] = 0;
		}
	}
	
	states_.push_back({ stateAction, 0 });



	if (pathNumber_ == 0)
	{
		//std::cout << "\t\t" << outLayer_.values_[0][0] << " " << outLayer_.values_[0][1] << " : " << action << "\n";
	}

	return action;
}





void Agent::reward(double value)
{
	states_[states_.size() - 1].reward = value;
}




void Agent::learn()
{
	if (pathNumber_ == 0)
	{
		//	clear the delta values
		for (unsigned i = 0; i < nLayers_ - 1; i++)
		{
			deltaWeights_[i] *= 0.0;
			deltaBiases_[i] *= 0.0;
		}
	}


	//	backpropagate all the actions
	for (int i = states_.size() - 1; i >= 0; i--)
	{
		//	clearing the trajectory deltas
		for (unsigned i = 0; i < nLayers_ - 1; i++)
		{
			trajectoryDeltaWeights_[i] *= 0.0;
			trajectoryDeltaBiases_[i] *= 0.0;
		}

		
		//	loss function derivative times softmax derivative
		Matrix intermediateDelta = 1.0 - states_[i].output;
		

		double stateReward = states_[i].reward;

		
		//	add all the future discounted rewards
		for (unsigned j = i; j < states_.size(); j++)
		{
			stateReward += states_[j].reward;
			states_[j].reward *= discountFactor_;
		}
		

		backpropagate(trajectoryDeltaWeights_, trajectoryDeltaBiases_, intermediateDelta);



		for (unsigned i = 0; i < nLayers_ - 1; i++)
		{
			deltaWeights_[i] += trajectoryDeltaWeights_[i] * (1.0 / states_.size()) * stateReward;
			deltaBiases_[i] += trajectoryDeltaBiases_[i] * (1.0 / states_.size()) * stateReward;
		}
	}

	pathNumber_++;
	

	//	apply delta for the whole batch
	if (pathNumber_ == pathBatchSize_)
	{
		for (unsigned i = 0; i < nLayers_ - 1; i++)
		{
			weights_[i] *= 0.9999;

			weights_[i] += deltaWeights_[i] * (learningRate_ / pathNumber_);
			biases_[i] += deltaBiases_[i] * (learningRate_ /  pathNumber_);
		}

		pathNumber_ = 0;
	}
	
	states_.clear();
}





unsigned Agent::pickAction()
{
	double adjust = 1.0;

	for (unsigned action = 0; action < outLayer_.xSize_; action++)
	{
		if ((((double)rand()) / (double)(RAND_MAX)) <= outLayer_.values_[0][action] / adjust)
		{
			return action;
		}

		adjust -= outLayer_.values_[0][action];
	}
	
}
