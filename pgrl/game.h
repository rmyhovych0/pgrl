#pragma once
#include <SFML/Graphics.hpp>
#include "agent.h"

class Game
{
public:
	Game();
	~Game();

	unsigned playHuman();

	unsigned playLearn(Agent& agent, bool display);
	unsigned play(Agent& agent);


private:

	bool hit();

	bool move();
	void draw();

	void reset();

private:

	int screenWidth_;
	int screenHeight_;


	float movementForce_;

	float ballRadius_;
	float ballPos_;

	float obstacleWidth_;

	sf::Vector2f obstaclePos_[2];
	float obstacleSpeed_;

	sf::RenderWindow* window_;

	sf::VertexArray obstacle_;
	sf::CircleShape ball_;
};

float len(const sf::Vector2f& vector);